<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\CaseItemController;
use App\Http\Controllers\CaseManageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resources([
	'leads'=>LeadController::class,
	'cases'=>CaseManageController::class,
	'caseItem'=>CaseItemController::class
]);

Route::get('/datetime-local', function() {
	return date("Y-m-d H:i:s",strtotime("2021-06-01 19:30:30"));
	return Carbon\Carbon::parse("2021-06-01 19:30:30")->toDateTimeLocalString();
	//php date function

});

Route::get('/lang/{locale}', function ($locale) {
    if (!in_array($locale, ['en', 'zh-CN'])) {
        abort(400);
    }

    App::setLocale($locale);
	

	$locale = App::currentLocale();

	return $locale;

});

//add new case route
// Route::get('/cases', [CaseManageController::class, 'index']);