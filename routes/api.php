<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\CaseItemController;
use App\Http\Controllers\CaseManageController;
use App\Models\CaseManage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//leads
Route::prefix('leads')->group(function () {
	Route::post('/bulkUpdate', [LeadController::class, 'bulkUpdate'])->name('bulkUpdate');
	Route::get('/getAllLeads', [LeadController::class, 'getAllLeads'])->name('getAllLeads');
	Route::post('/updateRemark', [LeadController::class, 'updateRemark']);
});

//cases
Route::prefix('cases')->group(function () {
	Route::get('getAllCases', [CaseManageController::class, 'getAllCases']);
	Route::get('getItemsById/{id}', [CaseManageController::class, 'getItemsById']);
	Route::get('caseChartData', [CaseManageController::class, 'caseChartData']);
});

//caseItem
Route::prefix('caseItem')->group(function () {
	Route::post('bulkUpdate', [CaseItemController::class, 'bulkUpdate']);
	Route::post('bulkDelete', [CaseItemController::class, 'bulkDelete']);
	Route::get('getAllCaseItemByCaseManageId/{case_manage_id}', [CaseItemController::class, 'getAllCaseItemByCaseManageId']);
});

Route::resources([
	'leads'=>LeadController::class,
	'cases'=>CaseManageController::class,
	'caseItem'=>CaseItemController::class
]);

