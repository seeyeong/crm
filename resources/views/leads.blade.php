@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card" style="height: 80vh">
				<div class="card-header">
					<h1>Leads Management</h1>
				</div>
				<div class="card-body table-responsive">
					<table class="table table-striped table-bordered ">
						<thead>
							<th>Name</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Remark</th>
							<th>Status</th>
							<th>Action</th>
						</thead>
						<tbody>
							@foreach ($leads as $lead)
								<tr>
									<td>{{$lead->name}}</td>
									<td>{{$lead->phone}}</td>
									<td>{{$lead->email}}</td>
									<td>{{$lead->remark}}</td>
									<td>{{$lead->leadStatus->name}}</td>
									<td><a href="/leads/{{$lead->id}}/edit" class="btn">Edit</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			{{-- <x-pagination :pagination="$leads" /> --}}
			{!! $leads->links() !!}
		</div>
	</div>
</div>
@endsection