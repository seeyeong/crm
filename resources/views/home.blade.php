@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <p class="mb-0">You are logged in!</p>
                </div>
            </div>
        </div>
    </div>
	<div class="row">
		<div class="col-4">
			<x-adminlte-info-box title="Leads" text="{{$totalLead}}" icon="fas fa-lg fa-users" icon-theme="primary" />
		</div>
		<div class="col-4">
			<x-adminlte-info-box title="Cases" :text="sprintf('%d/%d', $totalActiveCases, $totalCases)" icon="fas fa-lg fa-briefcase"
				icon-theme="primary"
				:progress="$activeCases" progress-theme="dark"
				:description="sprintf('%d%% of the cases active', $activeCases)"/>
		</div>
		<div class="col-4">
			<x-adminlte-info-box title="Tasks" :text="sprintf('%d/%d', $totalPendingItems, $totalItems)" icon="fas fa-lg fa-tasks"
				icon-theme="primary"
				:progress="$pendingItems" progress-theme="dark"
				:description="sprintf('%d%% of the tasks have been completed', $pendingItems)"/>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<div id="g2-chart"></div>
				</div>
			</div>
		</div>
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<canvas id="caseChart"></canvas>
				</div>
			</div>
		</div>
	</div>
@stop

@section('js')
	<script>
		// const labels = {!!json_encode($caseChartData['labels']) !!};
		// const config = {
		// 	type: 'bar',
		// 	data: {
		// 		labels: labels,
		// 		datasets: [{
		// 			label: 'Active',
		// 			backgroundColor: 'rgb(255, 99, 132)',
		// 			borderColor: 'rgb(255, 99, 132)',
		// 			data: @json($caseChartData['active']),
		// 			parsing: {
		// 				// xAxisKey: "month",
		// 				yAxisKey: 'active',
		// 			}
		// 		},{
		// 			label: 'Disabled',
		// 			backgroundColor: 'rgb(255, 233, 100)',
		// 			borderColor: 'rgb(255, 99, 132)',
		// 			data: @json($caseChartData['disabled']),
		// 			parsing: {
		// 				// xAxisKey: "month",
		// 				yAxisKey: 'disabled',
		// 			}
		// 		}]
		// 	},
		// };

		// //render chart
		// var caseChart = new Chart(
		// 	document.getElementById('caseChart'),
		// 	config
		// );

		const data = [
			{ month: 'Jan', name: 'sales', value: 38 },
			{ month: 'Feb', name: 'sales', value: 52 },
			{ month: 'Mar', name: 'sales', value: 61 },
			{ month: 'Apr', name: 'sales', value: 145 },
			{ month: 'May', name: 'sales', value: 48 },
			{ month: 'Jun', name: 'sales', value: 38 },
			{ month: 'Jul', name: 'sales', value: 38 },
			{ month: 'Aug', name: 'sales', value: 38 },
			{ month: 'Jan', name: 'expends', value: 25 },
			{ month: 'Feb', name: 'expends', value: 24 },
			{ month: 'Mar', name: 'expends', value: 11 },
			{ month: 'Apr', name: 'expends', value: 23 },
			{ month: 'May', name: 'expends', value: 27 },
			{ month: 'Jun', name: 'expends', value: 38 },
			{ month: 'Jul', name: 'expends', value: 22 },
			{ month: 'Aug', name: 'expends', value: 54 },
		];

		const chart = new G2.Chart({
			container: 'g2-chart',
			autoFit: true,
			height: 500,
		});

		chart.data(data);
		chart.scale('value', {
			nice: true,
		});

		chart.tooltip({
			showMarkers: false,
			shared: true
		});

		chart.interaction('active-region');
		chart.interval().position('month*value').color('name').adjust([
			{
			type: 'dodge',
			marginRatio: 0,
			},
		]);

		// 添加文本标注
		data.forEach((item) => {
			let offsetX = item.name == 'expends' ? 15 : -15;
			chart
				.annotation()
				.text({
					position: [item.month, item.value],
					content: item.value,
					style: {
						textAlign: 'center',
					},
					offsetX: offsetX,
					offsetY: -30,
				})
				.text({
					position: [item.month, item.value],
					content: item.month,
					style: {
						textAlign: 'center',
					},
					offsetX: offsetX,
					offsetY: -12,
				});
		});
		chart.render();

	</script>
@endsection