<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>
<body class="hold-transition sidebar-mini">
    <div id="app" class="wrapper">
		<x-navbar />
		@auth
			<x-sidebar />
		@endauth
		<div class="content">
			<div class="container-fluid">
				<div class="col-lg-9 offset-lg-3">
					@yield('content')
				</div>
			</div>
		</div>
		<x-footer />
    </div>
</body>
<!-- Scripts -->
<script src="{{ mix('js/app.js') }}" defer></script>
</html>
