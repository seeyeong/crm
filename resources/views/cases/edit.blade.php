@extends('adminlte::page')

@section('plugins.Datatables', true)

{{-- change the title --}}
@section('title', 'Edit Cases')

{{-- header in content --}}
@section('content_header')
    <h6>Edit Cases - {{$cases->id}}</h6>
@stop

@section('content')
	<div class="container">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<form action="/cases/{{$cases->id}}" method="POST">
						@method("PUT")
						@csrf
						<x-forms.input label="Lead" :value="$cases->lead->name" readonly="true" />
						<x-forms.input label="Name" placeholder="input case name" name="name" :value="$cases->name" />
						<x-forms.textarea label="Remark" row="3" name="remark" :value="$cases->remark" />
						<x-forms.select name="case_statuses_id" :value="$cases->case_statuses_id" :select="$caseStatuses" />
						<button class="btn btn-primary">Update</button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col">
							<h2>Case Items</h2>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="toolbar">
						<div class="form-inline" role="form">
							<a href="/caseItem/create?case_manage_id={{$cases->id}}" class="btn btn-primary float-right mr-2">Add</a>
							<div class="btn-group mr-2"> 
								@foreach ($caseItemStatuses as $status)
									<button id="show" class="btn btn-outline-primary" onclick="bulkUpdate({{$status->id}})">{{$status->name}}</button>
								@endforeach
							</div>
							<button class="btn btn-danger" onclick="bulkDelete()">Delete</button>
						</div>
					</div>
					<table 
						id="table"
						data-toggle="table" 
						data-search="true"
						data-show-export="true"
						data-export-data-type="all"
						data-url="/api/caseItem/getAllCaseItemByCaseManageId/{{$cases->id}}"
						data-pagination="true"
						data-site-pagination="server"
						data-server-sort="false"
						data-sticky-header="true"
						data-sticky-header-offset-top="100"
						data-toolbar="#toolbar"
						data-click-to-select="true"
					>
						<thead>
							<tr>
								<th data-checkbox="true"></th>
								<th data-sortable="true" data-field="id">Id</th>
								<th data-sortable="true" data-field="name">name</th>
								<th data-sortable="true" data-field="order_date">order date</th>
								<th data-sortable="true" data-field="execute_date">execute date</th>
								<th data-sortable="true" data-field="case_item_statuses_id">status</th>
								<th data-sortable="true" data-field="created_at">created at</th>
								<th data-sortable="true" data-field="updated_at">updated at</th>
								<th data-formatter="EditFormatter">Edit</th>
								<th data-formatter="DeleteFormatter">Delete</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop


@section('js')
    <script>
		//declare id="table"
		var $table = $('#table')

		function bulkUpdate(status) {
			var id = [];
			//use bootstrapTable methods 'getSelections' list of array
			//map the array of each element 
			$table.bootstrapTable('getSelections').map((value, index) => {
				id.push(value.id);
			})

			$.post("/api/bulkUpdate", {id: id, status: status}, function(data, status){
				alert("Data: " + data + "\nStatus: " + status);
				$table.bootstrapTable('refresh')
			});
		}
		function bulkDelete() {
			var id = [];
			$table.bootstrapTable('getSelections').map((value, index) => {
				id.push(value.id);
			})

			$.post("/api/bulkDelete", {id: id}, function(data, status){
				alert("Delete: " + data + "\nStatus: " + status);
				location.reload();
			});
		}

		function EditFormatter(value, row) {
			return '<a href="/caseItem/'+row.id+'/edit" class="btn btn-primary btn-sm">Edit</a>';
		}

		function DeleteFormatter(value, row) {
			return '<button type="submit" class="btn btn-danger btn-sm" onclick="mydelete('+row.id+')">Delete</button>';
		}

	</script>
@stop