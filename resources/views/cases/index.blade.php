{{-- extends the main layout --}} 
@extends('adminlte::page') 

{{-- change the title --}} 
@section('title', 'Cases')

{{-- header in content --}} 
@section('content_header') 
<h3>Cases Management</h3>
@stop 

{{-- content --}} 
@section('content') 
	<div class="container">
		<div> 
			@if (session()->has('status')) 
				<div class="alert alert-success"> 
					{{ session('status') }} 
				</div> 
			@endif 
		</div>
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div id="toolbar">
						<div class="form-inline" role="form">
							<a href="/cases/create" class="btn btn-primary float-right mr-2 btn-sm">Add</a>
							<button class="btn btn-danger btn-sm" onclick="bulkDelete()">Delete All</button>
						</div>
					</div>
					<div class="table-responsive">
						<table 
							id="table-index"
							class="table table-striped"
							data-toggle="table"
							data-search="true"
							data-url="/api/cases/getAllCases" {{-- 当你使用data-url的时候，thead里面要用data-feild去拿资料 --}}
							data-side-pagination="server"
							data-pagination="true"
							data-toolbar="#toolbar"
							data-sticky-header="true"
							data-sticky-header-offset-y="50"
							data-show-columns="true"
							data-show-columns-toggle-all="true"
							data-filter-control="true"
							data-click-to-select="true"
							data-id-field="id"
							data-server-sort="false"
						>
							<thead >
								<th data-checkbox="true"></th>
								<th data-field="id">ID</th>
								<th data-field="name" data-filter-control="input">Case Name</th>
								<th data-field="lead.name" data-filter-control="input">Lead</th>
								<th data-field="items" data-formatter="itemFormatter" >Items</th>
								<th data-field="remark">Remark</th>
								<th data-field="status.name" data-filter-control="select">Status</th>
								<th data-field="created_at">Created At</th>
								<th data-field="updated_at">Updated At</th>
								<th data-formatter="EditFormatter">Edit</th>
								<th data-formatter="DeleteFormatter">Delete</th>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div> 
@stop 

@section('js')
<script>

	function EditFormatter(value, row) {
		return '<a href="/cases/'+row.id+'/edit" class="btn btn-primary btn-sm">Edit</a>';
	}

	function DeleteFormatter(value, row) {
		return '<button type="submit" class="btn btn-danger btn-sm" onclick="mydelete('+row.id+')">Delete</button>';
	}

	function itemFormatter(value, row) {
		return value.length;
	}
</script> 
@stop