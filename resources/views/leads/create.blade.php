@extends('adminlte::page')

{{-- change the title --}}
@section('title', 'Leads')

{{-- header in content --}}
@section('content_header')
    <h1>Create Leads</h1>
@stop

@section('plugins.BsCustomFileInput', true)

@section('content')
<div class="card">
	<div class="card-body">
		<form action="/leads" method="POST" enctype="multipart/form-data">
			@csrf
			{{-- With label and feedback disabled --}}
			{{-- <x-adminlte-input-file name="avatar" label="Upload file" placeholder="Choose a file..." disable-feedback/> --}}
			<div class="mb-3 col-6">
				<label for="avatar">Profile Picture</label>
				<input type="file" class="dropify" name="avatar" data-allowed-file-extensions="png jpg jpeg webp" />
			</div>

			<div class="mb-3">
				<label for="name">Name</label>
				<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="name" value="{{old('name')}}">
				@error('name')
					<div class="invalid-feedback">{{ $message }}</div>
				@enderror
			</div>
			<div class="mb-3">
				<label for="Phone">Phone</label>
				<input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="Phone" placeholder="+0167654321" value="{{old('phone')}}">
				@error('phone')
					<div class="invalid-feedback">{{ $message }}</div>
				@enderror
			</div>
			<div class="mb-3">
				<label for="email" class="form-label">Email address</label>
				<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="name@example.com" value="{{old('email')}}">
				@error('email')
					<div class="invalid-feedback">{{ $message }}</div>
				@enderror
			</div>
			<div class="mb-3">
				<label for="remark" class="form-label">Remark</label>
				<textarea class="form-control" id="remark" name="remark" rows="3">{{old('remark')}}</textarea>
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
</div>
@endsection

@section('js')
	<script>
		$('.dropify').dropify();
	</script>
@endsection