@extends('adminlte::page')

{{-- change the title --}}
@section('title', 'Leads')

{{-- header in content --}}
@section('content_header')
    <h1>Edit Leads</h1>
@stop

@section('content')
	<div class="container">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<form action="/leads/{{$lead->id}}" method="POST" enctype="multipart/form-data">
						@method('PATCH')
						@csrf
						{{-- <x-adminlte-input-file name="avatar" label="Upload file" placeholder="Choose a file..." disable-feedback/> --}}
						<div class="mb-3 col-6">
							<label for="avatar">Profile Picture</label>
							<input type="file" class="dropify" name="avatar" data-allowed-file-extensions="png jpg jpeg" data-default-file="{{$lead->url ? "/{$lead->url}" : 'https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'}}" />
						</div>
						<div class="mb-3">
							<label for="name">Name</label>
							<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="name" value="{{old('name') ? old('name') : $lead->name}}">
							@error('name')
								<div class="invalid-feedback">{{ $message }}</div>
							@enderror
						</div>
						<div class="mb-3">
							<label for="Phone">Phone</label>
							<input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="Phone" placeholder="+0167654321" value="{{old('phone') ? old('phone') : $lead->phone}}">
							@error('phone')
								<div class="invalid-feedback">{{ $message }}</div>
							@enderror
						</div>
						<div class="mb-3">
							<label for="email" class="form-label">Email address</label>
							<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="name@example.com" value="{{old('email') ? old('email') : $lead->email}}">
							@error('email')
								<div class="invalid-feedback">{{ $message }}</div>
							@enderror
						</div>
						<div class="mb-3">
							<label for="remark" class="form-label">Remark</label>
							<textarea class="form-control" id="remark" name="remark" rows="3">{{old('remark') ? old('remark') : $lead->remark}}</textarea>
						</div>
						<div class="mb-3">
							<select class="form-select form-control" name="lead_status_id" aria-label="Default select example">
								@foreach ($leadStatus as $status)
									<option {{$lead->lead_status_id === $status->id ? "selected" : ""}} value="{{$status->id}}">{{$status->name}}</option>
								@endforeach
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script>
		var $dropify = $('.dropify').dropify();
		$dropify.on('dropify.error.imageFormat', function(event, element){
			alert('Image format error message!');
		});

		$dropify.on('dropify.afterClear', function(event, element){
			alert('File deleted');
		});
	</script>
@endsection