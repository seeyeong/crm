{{-- extends the main layout --}} 
@extends('adminlte::page') 
{{-- change the title --}} 
@section('title', 'Leads')
{{-- header in content --}} 
@section('content_header') 
<h1>Leads Management</h1>
@stop 
{{-- content --}} 
@section('content') 
<div class="card">
	<div class="card-body">
		<div class="table-responsive">
			<div id="toolbar">
				<div class="form-inline" role="form">
					<a href="/leads/create" class="btn btn-primary float-right mr-2"><i class="fas fa-fw fa-plus"></i> Add</a>
					<div class="btn-group mr-2"> 
						@foreach ($statuses as $status)
							<button id="show" class="btn btn-outline-primary" onclick="bulkUpdate({{$status->id}})">{{$status->name}}</button>
						@endforeach
					</div>
					{{-- <button class="btn btn-danger btn-sm" onclick="bulkDelete()">Delete</button> --}}
				</div>
			</div>
			<div class="table-responsive">
				<table 
					id="table-leads"
					class="table table-striped"
					data-toggle="table"
					data-url="/api/leads/getAllLeads"
					data-side-pagination="server"
					data-pagination="true"
					data-sticky-header="true"
					data-sticky-header-offset-y="50"
					data-toolbar="#toolbar"
					data-click-to-select="true"
					data-show-columns="true"
					data-show-columns-toggle-all="true"
					data-search="true"
					data-id-field="id"
					data-use-pipeline="true"
					data-pipeline-size="500"
					data-show-print="true"
					data-show-export="true"
					data-export-data-type="all"
					data-smart-display="true"
					data-thead-classes="thead-light"
					data-filter-control="true"
					data-show-search-clear-button="true"
					data-server-sort="false"
					data-editable-url="api/leads/updateRemark"
					>
					<thead >
						<th data-checkbox="true"></th>
						<th data-field="url" data-formatter="avatarFormatter">{{__('adminlte::adminlte.avatar')}}</th>
						<th data-sortable="true" data-field="name" data-filter-control="input">Name</th>
						<th data-field="phone" data-filter-control="input">Phone</th>
						<th data-field="email" data-filter-control="input">Email</th>
						<th data-field="remark" data-editable="true" data-type="textarea">Remark</th>
						<th data-sortable="true" data-field="cases_count">Cases</th>
						<th data-sortable="true" data-field="lead_status.name" data-filter-control="select">Status</th>
						<th data-sortable="true" data-field="created_at" data-formatter="createdAtFormatter">Created At</th>
						<th data-sortable="true" data-field="updated_at">Updated At</th>
						<th data-formatter="actionFormatter">Action</th>
					</thead> 
				</table>
			</div>
		</div>
	</div>
</div>
@stop 

@section('js')
<script>
	var $table = $('#table-leads')

	function actionFormatter(value, row) {
		return [
			'<div class="row">',
			'<a href="/leads/'+row.id+'/edit" class="col btn btn-primary btn-xs mx-1"><i class="fas fa-edit"></i></a>',
			'<button type="submit" class="col btn btn-outline-danger btn-xs mx-1" onclick="mydelete('+row.id+')"><i class="fas fa-trash"></i></button>',
			'</div>'
		].join('');
	}

	function mydelete(id) {
		console.log(id)
		//use sweetalert2
		// Swal.fire({
		// 		title: 'Are you sure?',
		// 		text: "You won't be able to revert this!",
		// 		icon: 'warning',
		// 		showCancelButton: true,
		// 		confirmButtonColor: '#3085d6',
		// 		cancelButtonColor: '#d33',
		// 		confirmButtonText: 'Yes, delete it!'
		// 	}).then((result) => {
		// 		//when click on confirm button
		// 		if (result.value) {
		// 			//http request post to server to delete
		// 			$.ajax({
		// 				type: "DELETE",
		// 				url: "/leads/"+id,
		// 				success: function(res) {
		// 					if(res) {
		// 						//display success alert
		// 						Swal.fire(
		// 							'Deleted!',
		// 							'Your case has been deleted.',
		// 							'success'
		// 						).then(({value}) => value && location.reload())
		// 					}
		// 				}
		// 			})
		// 		}
		// 	})
	}

	function bulkUpdate(status) {
		// alert('getSelections: ' + JSON.stringify($table.bootstrapTable('getSelections')))
		let array = [];
		$table.bootstrapTable('getSelections').map((value, index) => {
			array.push(value.id)
		})
		// [9849,9845]
		$.post('/api/leads/bulkUpdate', {id: array, status}, (data, status) => {
			if(status == 'success') {
				$(document).Toasts('create', {
					title: status,
					body: 'Updated successful.',
					autohide: true,
					class: 'bg-success',
					delay: 2500
				})
			}
			$table.bootstrapTable('refreshOptions', {url: 'api/leads/getAllLeads'})
		})
	}

	function avatarFormatter(value) {
		return value && '<img src="'+value+'" class="rounded mx-auto d-block" width="80" height="auto">';
	}

	function createdAtFormatter(value) {
		return 
	}
	
	$(document).ready(function() {
		$.fn.editable.defaults.mode = 'inline';
		$.fn.editable.defaults.type = 'textarea';
		$.fn.editableform.template = '<form class="form-inline editableform"><div class="control-group"><div><div class="editable-input"></div><br/><div class="editable-buttons"></div></div><div class="editable-error-block"></div></div></form>';
		$.fn.editableform.buttons = '<button type="submit" class="btn btn-xs btn-primary editable-submit"><i class="fas fa-edit"></i></button><button type="button" class="btn btn-xs btn-danger editable-cancel"><i class="fas fa-minus"></i></button>';
	});
</script>
@stop