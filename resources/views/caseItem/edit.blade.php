@extends('adminlte::page')

{{-- change the title --}}
@section('title', 'Create Case Item')

{{-- header in content --}}
@section('content_header')
    <h1>Edit Case Item - {{$caseItem->id}}</h1>
@stop

@section('content')
	<div class="container">
		<div class="card">
			<div class="card-body">
				<form action="/caseItem/{{$caseItem->id}}" method="POST">
					@method("PATCH")
					@csrf
					<x-forms.input label="Name" name="name" :value="$caseItem->name"/>
					<x-forms.input label="Order Date" name="order_date" :value="$caseItem->order_date" type="datetime-local" />
					<x-forms.input label="Execute Date" name="execute_date" :value="$caseItem->execute_date" type="datetime-local" />
					<x-forms.select label="Status" name="case_item_statuses_id" :select="$statuses" :value="$caseItem->case_item_statuses_id" />

					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</div>
@stop