@extends('adminlte::page')

{{-- change the title --}}
@section('title', 'Create Case Item')

{{-- header in content --}}
@section('content_header')
    <h1>Create Case Item</h1>
@stop

@section('content')
	<div class="container">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<div class="card">
			<div class="card-body">
				<form action="/caseItem" method="post">
					@csrf
					<input type="hidden" name="case_manage_id" value="{{request('case_manage_id')}}">
					<x-forms.input label="Name" name="name"/>
					<x-forms.input type="datetime-local" label="Order Date" name="order_date" />
					<button class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</div>
@endsection