@extends('adminlte::page')

@section('title', 'Cases')

@section('content_header')
    <h1>Cases</h1>
@stop

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h1>Case Manage</h1>
					</div>
					<div class="card-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>lead</th>
									<th>name</th>
									<th>remark</th>
									<th>status</th>
								</tr>
							</thead>
							<tbody>
								{{-- Array as elements --}}
								@foreach ($caseManage as $item)
									<tr>
										<td>{{$item->lead ? $item->lead->name : ''}}</td>
										<td>{{$item->name}}</td>
										<td>{{$item->remark}}</td>
										<td>{{$item->status->name}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

			{{ $caseManage->links()}}
		</div>
	</div>
@endsection