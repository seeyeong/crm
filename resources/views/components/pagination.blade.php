<nav aria-label="Page navigation example">
	<ul class="pagination justify-content-end">		
		@foreach ($pagination as $index => $link)
			@if ($index == 0)
				<li class="page-item disabled">
					<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
				</li>
			@endif
			@if ($index == count($pagination)-1 )
				<li class="page-item">
					<a class="page-link" href="#">Next</a>
				</li>
			@endif
			<li class="page-item"><a class="page-link" href="#">{{$link}}</a></li>
		@endforeach
		
	</ul>
</nav>