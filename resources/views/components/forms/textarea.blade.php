<div class="mb-3">
	<label for="{{$name}}" class="form-label">{{$label}}</label>
	<textarea class="form-control" id="{{$name}}" name="{{$name}}" rows="{{$row}}">{{old("$name") ? old("$name") : $value}}</textarea>
</div>