<div class="mb-3">
	<label for="{{$name}}">{{$label}}</label>
	<input type="{{$type}}" list="datalistOptions" class="form-control @error("$name") is-invalid @enderror" 
		name="{{$name}}" 
		id="{{$name}}" 
		placeholder="{{$placeholder ? $placeholder : "type to search..."}}" 
		value="{{old("$name") ? old("$name") : $value}}"
		{{$readonly ? "readonly" : ''}}
	>
	<datalist id="datalistOptions">
		<option value="San Francisco">
		<option value="New York">
		<option value="Seattle">
		<option value="Los Angeles">
		<option value="Chicago">
	</datalist>

	@error("$name")
		<div class="invalid-feedback">{{ $message }}</div>
	@enderror

</div>