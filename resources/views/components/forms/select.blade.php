<div class="mb-3">
	<label for="{{$name}}">{{$label}}</label>
	<select id="{{$name}}" class="form-select form-control" name="{{$name}}" aria-label="Default select example">
		@foreach ($select as $item)
			<option {{$value === $item->id ? "selected" : ""}} value="{{$item->id}}">{{$item->name}}</option>
		@endforeach
	</select>
</div>