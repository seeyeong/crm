<?php 

return [
	'success' => [
		'create' => ':name create successful',
		'update' => ':name update successful',
	]
];