<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Select extends Component
{
	public $label;
	public $name;
	public $select;
	public $value;
    
	/**
	 * Create select instances
	 *
	 * @param string $name
	 * @param array $values
	 * @param string $values
	 */
    public function __construct($label = "you forgot to put label", $name, $select, $value)
    {
		$this->label = $label;
		$this->name = $name;
		$this->select = $select;
		$this->value = $value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.select');
    }
}
