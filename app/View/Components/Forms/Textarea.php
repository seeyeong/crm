<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Textarea extends Component
{
	public $label;
	public $name;
	public $value;
	public $row;
    
	/**
	 * Create textarea instance
	 *
	 * @param string $label
	 * @param string $name
	 * @param string $value
	 * @param string $row
	 */
    public function __construct($label, $name, $value, $row)
    {
		$this->label = $label;
		$this->name = $name;
		$this->value = $value;
		$this->row = $row;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.textarea');
    }
}
