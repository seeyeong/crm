<?php

namespace App\View\Components\Forms;

use Carbon\Carbon;
use Illuminate\View\Component;
use phpDocumentor\Reflection\Types\Boolean;

class Input extends Component
{
	public $type;
	/**
	 * Input label
	 *
	 * @var string
	 */
	public $label;

	/**
	 * Input name
	 *
	 * @var string
	 */
	public $name;

	/**
	 * Input placeholder
	 *
	 * @var string
	 */
	public $placeholder;

	/**
	 * Error 
	 *
	 * @var object
	 */
	public $error;
	
	/**
	 * Input value
	 *
	 * @var [string, numberic, alphabetnumeric]
	 */
	public $value;

	/**
	 * Input readonly
	 *
	 * @var boolean
	 */
	public $readonly;

	/**
	 * Create input component instance
	 *
	 * @param string $label
	 * @param string $name
	 * @param string $placeholder
	 * @param string $message
	 * @param [string, numberic, alphabetnumeric] $value
	 * @param Boolean $readonly
	 */
    public function __construct($type = "text", $label, $name = null, $placeholder = null, $value = null, $readonly = false)
    {
		$this->type = $type;
        $this->label = $label;
        $this->name = $name;
        $this->placeholder = $placeholder;
        // $this->error = $error;
        $this->value = $value;
		if($type == 'datetime-local') {
			$this->value = Carbon::now()->toDateTimeLocalString();
		}
		$this->readonly = $readonly;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.input');
    }
}
