<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    use HasFactory;
	
	// protected $fillable = ['name','phone','email','remark','lead_status_id'];
	protected $guarded = [];

	/**
	 * Get the leadStatus that owns the Lead
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function leadStatus()
	{
		return $this->belongsTo(LeadStatus::class, 'lead_status_id');
	}

	/**
	 * Get all of the cases for the Lead
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function cases()
	{
		return $this->hasMany(CaseManage::class);
	}
}
