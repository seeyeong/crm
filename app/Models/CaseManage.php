<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CaseManage extends Model
{
    use HasFactory;
	protected $guarded = [];
	/**
	 * Get the lead that owns the CaseManage
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function lead()
	{
		return $this->belongsTo(Lead::class);
	}

	/**
	 * Get all of the items for the CaseManage
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function items()
	{
		return $this->hasMany(CaseItem::class);
	}

	/**
	 * Get the status that owns the CaseManage
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function status()
	{
		return $this->belongsTo(CaseStatus::class, 'case_statuses_id');
	}
}
