<?php

namespace App\Models;

use App\Models\CaseItemStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CaseItem extends Model
{
    use HasFactory;

	protected $guarded = [];
	/**
	 * Get the caseItemStatus that owns the CaseItem
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function status()
	{
		return $this->belongsTo(CaseItemStatus::class, 'case_item_statuses_id');
	}
}
