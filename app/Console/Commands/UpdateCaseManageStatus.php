<?php

namespace App\Console\Commands;

use App\Models\CaseManage;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateCaseManageStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateCasesStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this is for update cases status for testing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		print("update case\n");
		$max = CaseManage::count('id');
		foreach ([3,2,1] as $mth) {
			//random days in month
			$d = cal_days_in_month(CAL_GREGORIAN,date('m', strtotime("-$mth months")),date('Y'));
			$arrDays = [];
			for ($i=0; $i < rand(7, 20); $i++) { 
				$arrDays[] = rand(1, $d);
			}
			$arrDays = array_unique($arrDays);
			sort($arrDays);
			
			$id = [];
			foreach ($arrDays as $day) {
				for ($i=0; $i < rand(100, 2000); $i++) { 
					$id[] = rand(14, $max);
				}
				$day = date('Y-m-d H:i:s', strtotime("-$day days -$mth months 2021"));
				$sql = "update case_manages set created_at='$day', updated_at='$day' where id in (".implode(",", array_unique($id)).")";
				$updated = DB::update($sql);
				if($updated) {
					print("update $day success \n");
				}
			}
		}
    }
}
