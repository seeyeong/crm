<?php

namespace App\Http\Controllers;

use App\Models\CaseItem;
use App\Models\CaseManage;
use App\Models\CaseStatus;
use Illuminate\Http\Request;
use App\Models\CaseItemStatus;
use App\Models\Lead;
use Illuminate\Support\Facades\DB;

class CaseManageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$cases = CaseManage::with(['lead','items.status', 'status']);
		//check if user got search keywords
		if ($request->has('search')) {
			$search = $request->search;
			//model using where to query data
			$cases = $cases->where('name', 'like', "%{$search}%")
					->withQueryString()->paginate();
		} else {
			$cases = $cases->paginate();
		}

		$cases = ["total"=>$cases->total(), "rows"=> $cases->items()];
		// return $cases;
		
		return view('cases.index', compact('cases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CaseManage  $case
     * @return \Illuminate\Http\Response
     */
    public function show(CaseManage $case)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CaseManage  $case
     * @return \Illuminate\Http\Response
     */
    public function edit(CaseManage $case)
    {
		$caseStatuses = CaseStatus::all();
		$caseItemStatuses = CaseItemStatus::all();
		$items = CaseItem::where('case_manage_id', $case->id)->orderByDesc('updated_at')->orderByDesc('created_at')->paginate(10);

        return view('cases.edit', ['cases'=>$case,'items'=>$items, 'caseStatuses'=>$caseStatuses, 'caseItemStatuses' => $caseItemStatuses]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CaseManage  $case
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CaseManage $case)
    {
        $case->update(
			$request->validate([
				'name' => 'required|min:6',
				'remark' => 'nullable',
				'case_statuses_id' => 'required|exists:case_statuses,id'
			])
		);

		if($case->wasChanged()) {
			return redirect()->back()->with('status', "Cases updated successful");
		}else{
			return redirect()->back();
		}

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CaseManage  $case
     * @return \Illuminate\Http\Response
     */
    public function destroy(CaseManage $case)
    {
		return $case->delete();
    }

	public function getAllCases(Request $request)
	{
		$string = [];
		if($request->filter) {
			foreach(json_decode($request->filter) as $key => $value) {
				switch ($key) {
					case 'status.name':
						$string[] = "case_statuses_id = ".CaseStatus::where('name', $value)->first()->id;
						break;
					case 'lead.name':
						$string[] = 'lead_id in ('.implode(",", Lead::where('name', 'like', "%$value%")->pluck('id')->toArray()).")";
						break;
					default:
						$string[] = "{$key} like '%{$value}%'";
						break;
				}
			};
		}
		$cases = CaseManage::with(['lead','items.status', 'status']);
		//check if user got search keywords
		if ($request->has('search') && $request->search !== null) {
			$search = $request->search;
			$cases = $cases->where('name', 'like', "%{$search}%")
						->paginate($request->limit)->withQueryString();
			//model using where to query data
		} else if ($request->filter !== null) {
			$cases = $cases->whereRaw(implode(' and ', $string))
					->paginate($request->limit)->withQueryString();
		}else {
			$cases = $cases->paginate($request->limit);
		}

		return ["total"=>$cases->total(), "rows"=> $cases->items()];
	}

	public function getItemsById($id)
	{
		return CaseItem::with('status')->where('case_manage_id', $id)->orderByDesc('updated_at')->orderByDesc('created_at')->paginate(10);
	}

	public function caseChartData() 
	{
		$select = DB::select("SELECT count(id) AS total, case_statuses_id, substring(updated_at, 1, 7) AS `month` FROM case_manages GROUP BY substring(updated_at,1,7),case_statuses_id ORDER BY substring(updated_at,1,7);");

		$formattedData = [];
		$active = [];
		$disabled = [];
		foreach ($select as $data) {
			$mth = date('M', strtotime($data->month));
			$formattedData[$mth]['month'] = $mth;
			if($data->case_statuses_id == 1) {
				$label = "active";
				$active[] = $data->total;
			}else{
				$label = "disabled";
				$disabled[] = $data->total;
			}
			$formattedData[$mth][$label] = $data->total;
		}

		$keys = array_keys($formattedData);
		$data = array_values($formattedData);

		return ["labels" => $keys, "data"=>$data, 'active'=>$active, 'disabled'=>$disabled];
	}
}
