<?php

namespace App\Http\Controllers;

use App\Models\CaseItem;
use Illuminate\Http\Request;
use App\Models\CaseItemStatus;
use Illuminate\Support\Facades\DB;

class CaseItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('caseItem.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        CaseItem::create($request->validate([
			'case_manage_id' => 'required|exists:case_manages,id',
			'name' => 'required|min:4',
			'order_date' => 'required|date',
		]));

		return redirect("/cases/{$request->case_manage_id}/edit");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CaseItem  $caseItem
     * @return \Illuminate\Http\Response
     */
    public function show(CaseItem $caseItem)
    {
        return $caseItem;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CaseItem  $caseItem
     * @return \Illuminate\Http\Response
     */
    public function edit(CaseItem $caseItem)
    {	
		$statuses = CaseItemStatus::all();
        return view ('caseItem.edit', ['caseItem' => $caseItem, 'statuses' => $statuses]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CaseItem  $caseItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CaseItem $caseItem)
    {
		$caseItem->update(
			$request->validate([
				'name'=>'required|min:6',
				'order_date'=>'date',
				'execute_date'=>'date',
				'case_item_statuses_id'=>'numeric',
			])
		);

		return redirect("/cases/{$caseItem->case_manage_id}/edit")->with('status', 'Case item updated successful.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CaseItem  $caseItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(CaseItem $caseItem)
    {
        $caseItem->delete();

		return redirect()->back()->with('status', "Item '{$caseItem->name}' has been deleted.");
    }

	public function bulkUpdate(Request $request)
	{
		return DB::table('case_items')->whereIn('id', $request->id)->update([
			'case_item_statuses_id' => $request->status
		]);
	}

	public function bulkDelete(Request $request)
	{
		return DB::table('case_items')->whereIn('id', $request->id)->delete();
	}

	public function getAllCaseItemByCaseManageId(Request $request)
	{
		$items = CaseItem::where('case_manage_id', $request->case_manage_id)->orderByDesc('updated_at')->orderByDesc('created_at');
		if ($request->has('search') && $request->search !== null) {
			$items = $items->where('name', 'like', "%{$request->search}%")
						->paginate($request->limit)->withQueryString();
			//model using where to query data
		} else {
			$items = $items->paginate($request->limit);
		}

		return ["total"=>$items->total(), "rows"=>$items->items()];
	}
}
