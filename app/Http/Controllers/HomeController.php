<?php

namespace App\Http\Controllers;

use App\Models\CaseItem;
use App\Models\CaseManage;
use App\Models\Lead;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		//leads total
		$totalLead = Lead::count('id');
		//active cases
		$totalActiveCases = CaseManage::where('case_statuses_id', 1)->count("id");
		$totalCases = CaseManage::count("id");
		$activeCases = round($totalActiveCases / $totalCases * 100);
		//pending task
		$totalPendingItems = CaseItem::where('case_item_statuses_id', 1)->count('*');
		$totalItems = CaseItem::count('*');
		$pendingItems = round($totalPendingItems / $totalItems * 100);

		$CaseManageController = new CaseManageController;
		$caseChartData = $CaseManageController->caseChartData();

        return view('home', compact('totalLead', 'totalActiveCases', 'totalCases', 'activeCases', 'totalPendingItems', 'totalItems', 'pendingItems', 'caseChartData'));
    }
}
