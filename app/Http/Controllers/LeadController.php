<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\LeadStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class LeadController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$search = '';
		//if user got search something then query the data by keywords search...
		$leads = Lead::with('leadStatus')->withCount('cases')->orderByDesc('updated_at')->orderByDesc('created_at');
		//gat total lead count
		$totalLeads = Lead::count();
		//check if user got search keywords
		if ($request->has('search')) {
			$search = $request->search;
			//model using where to query data
			$leads = $leads->where('name', 'like', "%{$search}%")
					->orWhere('email', 'like', "%$search%")
					->orWhere('phone', 'like', "%$search%")
					->paginate(10)->withQueryString();
		} else {
			$leads = $leads->paginate(10);
		}

		$statuses = LeadStatus::all();

		$progress = ($leads->total() / $totalLeads) * 100;
		//return leads view and pass leads data to view
		return view('leads.index', compact('leads', 'search', 'totalLeads', 'progress', 'statuses'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('leads.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//check request is valid for storing
		$request->validate([
			'name' => 'required',
			'phone' => 'required|unique:leads',
			'email' => 'required|unique:leads',
		]);

		if($request->hasFile('avatar')) {
			$path = $request->file('avatar')->store('images/avatar');
		}
		
		$lead = Lead::create([
			'name' => $request->name,
			'phone' => $request->phone,
			'email' => $request->email,
			'remark' => $request->remark,
			'lead_status_id' => 1,
			'url' => $path,
			'avatar' => $request->avatar
		]);

		flash()->success(__('flash.success.create', ['name'=>$request->name]));
		return redirect()->route('leads.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Lead  $lead
	 * @return \Illuminate\Http\Response
	 */
	public function show(Lead $lead)
	{
		$lead = $lead->load(['leadStatus', 'cases']);
		return $lead;
		// return view('leads.show', compact('lead'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\Lead  $lead
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Lead $lead)
	{
		$leadStatus = LeadStatus::all();
		return view('leads.edit', compact('lead', 'leadStatus'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Lead  $lead
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Lead $lead)
	{
		$path = $lead->url;

		//when update and avatar is not existing in storage
		if($request->hasFile('avatar') && $request->file('avatar')->getClientOriginalName() != $lead->url) {
			$path = $request->file('avatar')->storeAs('images/avatar', $request->file('avatar')->getClientOriginalName());
		}
		
		//check if url not existing in storage
		if(!$request->hasFile('avatar') && !Storage::exists($lead->url)) $path = null;

		$validated = $request->validate([
			'name' => 'required',
			'phone' => 'required|unique:leads,phone,' . $lead->id,
			'email' => 'required|unique:leads,email,' . $lead->id,
			'remark' => 'nullable',
			'lead_status_id' => 'numeric',
			'avatar' => 'nullable|mimes:jpg,png,jpeg'
		]);

		//update to model
		$updated = $lead->update([
			'name' => $validated['name'],
			'phone' => $validated['phone'],
			'email' => $validated['email'],
			'remark' => $validated['remark'],
			'lead_status_id' => $validated['lead_status_id'],
			'url' => $path,
			'avatar' => isset($validated['avatar']) ? $validated['avatar'] : ''
		]);

		if($updated) {
			flash()->success('updated successful');
		}
		return redirect()->route('leads.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Lead  $lead
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Lead $lead)
	{
		$lead->delete();

		return redirect()->route('leads.index')->with('status', "The leads $lead->id has been deleted.");
	}

	public function leads()
	{
		return response()->json(Lead::with('leadStatus')->paginate(100));
	}

	public function getAllLeads(Request $request)
	{
		$string = [];
		if($request->filter) {
			foreach(json_decode($request->filter) as $key => $value) {
				if($key == 'lead_status.name') {
					$string[] = "lead_status_id = ".LeadStatus::where('name', $value)->first()->id;
				}else{
					$string[] = "{$key} like '%{$value}%'";
				}
			};
		}
		//if user got search something then query the data by keywords search...
		$leads = Lead::with('leadStatus')->withCount('cases')->orderByDesc('updated_at')->orderByDesc('created_at');
		//check if user got search keywords
		if ($request->searchText !== null && $request->filter == null) {
			//model using where to query data
			$leads = $leads->where('name', 'like', "%{$request->searchText}%")
					->orWhere('email', 'like', "%$request->searchText%")
					->orWhere('phone', 'like', "%$request->searchText%")
					->paginate($request->limit)->withQueryString();
			
		} else if($request->filter !== null) {
			$leads = $leads->whereRaw(implode(' and ', $string))
					->paginate($request->limit)->withQueryString();
		}else {
			$leads = $leads->paginate($request->limit);
		}
		return ["total"=>$leads->total(), "rows"=>$leads->items()];
	}

	public function bulkUpdate(Request $request)
	{
		return DB::table('leads')->whereIn('id', $request->id)->update([
			'lead_status_id' => $request->status
		]);
	}

	public function updateRemark(Request $request)
	{
		$request->validate([
			'name' => 'required',
			'pk' => 'required|numeric',
			'value' => 'required',
		]);

		return Lead::where('id', $request->pk)->update([
			$request->name => $request->value
		]);
	}
}
