<?php

namespace App\Http\Controllers;

use App\Models\CaseItemStatus;
use Illuminate\Http\Request;

class CaseItemStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CaseItemStatus  $caseItemStatus
     * @return \Illuminate\Http\Response
     */
    public function show(CaseItemStatus $caseItemStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CaseItemStatus  $caseItemStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(CaseItemStatus $caseItemStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CaseItemStatus  $caseItemStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CaseItemStatus $caseItemStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CaseItemStatus  $caseItemStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(CaseItemStatus $caseItemStatus)
    {
        //
    }
}
