<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCaseManageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('case_manages', function (Blueprint $table) {
			$table->string('name', 100)->change();
			$table->string('remark')->nullable()->change();
			$table->index(['lead_id', 'created_at'], 'case_manages_index');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('case_manages', function (Blueprint $table) {
			$table->string('name', 50)->change();
			$table->string('remark')->change();
			$table->dropIndex('case_manages_index');
		});
    }
}
