<?php

namespace Database\Seeders;

use App\Models\CaseItemStatus;
use Illuminate\Database\Seeder;

class CaseItemStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$statuses = ['pending','doing','completed'];
		foreach ($statuses as $status) {
			CaseItemStatus::create([
				'name'=>$status
			]);
		}
    }
}
