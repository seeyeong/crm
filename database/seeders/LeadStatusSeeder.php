<?php

namespace Database\Seeders;

use App\Models\LeadStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeadStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$datas = [['name'=>'active'],['name'=>'disabled'],['name'=>'pending']];
		foreach ($datas as $data) {
			LeadStatus::create(['name'=>'active']);
		}
    }
}
