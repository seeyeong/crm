<?php

namespace Database\Seeders;

use App\Models\CaseItem;
use App\Models\Lead;
use App\Models\CaseManage;
use Illuminate\Database\Seeder;

class CaseManageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// $faker = \Faker\Factory::create();
        CaseManage::factory()
		->count(50000)
		->hasItems(rand(3,10))
		->create();
    }
}
