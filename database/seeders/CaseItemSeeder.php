<?php

namespace Database\Seeders;

use App\Models\CaseItem;
use Illuminate\Database\Seeder;

class CaseItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CaseItem::factory()->count(5)->create();
    }
}
