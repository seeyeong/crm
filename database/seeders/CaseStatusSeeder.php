<?php

namespace Database\Seeders;

use App\Models\CaseStatus;
use Illuminate\Database\Seeder;

class CaseStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['active','disabled'];
		foreach ($statuses as $status) {
			CaseStatus::create(['name'=>$status]);
		}
    }
}
