<?php

namespace Database\Factories;

use App\Models\Lead;
use App\Models\CaseManage;
use Illuminate\Database\Eloquent\Factories\Factory;

class CaseManageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CaseManage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
		$max = Lead::count('id');
        return [
            'lead_id'=>rand(1, $max),
			'name'=>$this->faker->name,
			'remark'=>$this->faker->sentence(),
			'case_statuses_id'=>rand(1, 2)
        ];
    }
}
