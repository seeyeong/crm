<?php

namespace Database\Factories;

use App\Models\CaseItem;
use App\Models\CaseItemStatus;
use App\Models\CaseManage;
use Database\Seeders\CaseItemSeeder;
use Illuminate\Database\Eloquent\Factories\Factory;

class CaseItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CaseItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
		$items = ['Advertising', 'Facebook Marketing', 'Google Marketing', 'Video Production'];
		$date = $this->faker->dateTimeBetween('-365 days', 'now', 'Asia/Kuala_Lumpur')->format('Y-m-d H:i:s');
		$days = rand(1, 14);
		$hours = rand(1, 12);
		$edate = date('Y-m-d H:i:s', strtotime("$date +$days days +$hours hours"));
		$count = CaseItemStatus::count('id');
		$max = CaseManage::count('id');
        return [
            'name'=> $items[rand(0, 3)],
			'order_date'=> $date,
			'execute_date'=> $edate,
			'case_item_statuses_id'=>rand(1,$count)
        ];
    }
}
